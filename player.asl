/*******************************************************************************
* Agente: 	player.asl														   *
* Objetivo:	Jugar al Conecta Cuatro a ganar y a perder.						   *
* Autores: 																	   *	
*			Alexandre Villanueva Garca										   *
*			Cristhian Ferreiro Garrido										   *		
*			Adrin Araujo Gregorio											   *	
********************************************************************************/

/* Reglas: reglas lgicas necesarias para inferir la columna en la que se colocar la prxima fichas. */

//Recupera el nombre del jugador actual.
jugadorActual(Player) :- .my_name(N) & Player = N.

//Identifica al agente y al rival
valorJugadores(1,2) :- .my_name(N) & (N==player1).
valorJugadores(2,1) :- .my_name(N) & (N==player2).

//True si la columna C est llena
columnaLlena(C) :- tablero(C,0,D) & not D == 0.

//Infiere en N la fila de la primera casilla disponible en la columna C
filaLibre(C,N) :- .findall(F,tablero(C,F,0),L) & .max(L,N). 

//Infiere en Pos la columna que debe seleccionarse en el prximo turno en funcin del modo de juego (Estrategia)
funcionMovimiento(Pos,Estrategia) :- Estrategia == jugarAGanar & listaValores(L,Estrategia) & movimientoFinalGanar(L,Pos).
funcionMovimiento(Pos,Estrategia) :- Estrategia == jugarAPerder & listaValores(L,Estrategia) & movimientoFinalPerder(L,Pos).

//Infiere en Pos la columna seleccionada en jugarAGanar
movimientoFinalGanar(L,3) :- .max(L,Max) & .nth(3,L,Max).
movimientoFinalGanar(L,4) :- .max(L,Max) & .nth(4,L,Max).
movimientoFinalGanar(L,Pos) :- .max(L,Max) & .nth(Pos,L,Max).

//Infiere en Pos la columna seleccionada en jugarAPerder
movimientoFinalPerder(L,0) :- .min(L,Min) & .nth(0,L,Min).
movimientoFinalPerder(L,7) :- .min(L,Min) & .nth(7,L,Min).
movimientoFinalPerder(L,Pos) :- .min(L,Min) & .nth(Pos,L,Min).

//Infiere en L una lista con las puntuaciones de cada columna segn el modo de juego
listaValores(L,Estrategia) :- valorColumna(0,X0,Estrategia) & valorColumna(1,X1,Estrategia) & valorColumna(2,X2,Estrategia) & valorColumna(3,X3,Estrategia) & valorColumna(4,X4,Estrategia) & valorColumna(5,X5,Estrategia) & valorColumna(6,X6,Estrategia) & valorColumna(7,X7,Estrategia) & .concat([X0],[X1],[X2],[X3],[X4],[X5],[X6],[X7],L).

//valorColumna(C,X,E): Infiere en X la puntuacin de la columna C en funcin del modo de juego E
valorColumna(C,-1,Estrategia) :- Estrategia == jugarAGanar & columnaLlena(C).
valorColumna(C,X,Estrategia) :-  Estrategia == jugarAGanar & not columnaLlena(C) & valorJugadores(V1,V2) & valor(C,XP,V1) & valor(C,XR,V2) & .max([XP+0.1,XR],X).
valorColumna(C,7,Estrategia) :- Estrategia == jugarAPerder & columnaLlena(C).
valorColumna(C,X,Estrategia) :- Estrategia == jugarAPerder & not columnaLlena(C) & valorJugadores(V1,V2) & valor(C,X,V1).

//Infiere en R el nmero de fichas de Jugador que se alinearn si Jugador hace put(C)
valor(C,R,Jugador) :- filaLibre(C,F) & valorVert(C,F,Jugador,V) & valorHor(C,F,Jugador,H) &  valorAsc(C,F,Jugador,A) & valorDesc(C,F,Jugador,D) & .max([V,H,A,D],R).

//Infiere en R el nmero de fichas de Jugador que se alinearn en cada direccin (vertical, horizontal, diagonal ascendiente y diagonal descendiente) si Jugador hace put(C)
valorVert(C,F,Jugador,V) :- vertAbajo(C,F,Jugador,R1)& vertMedioAbajo(C,F,Jugador,R2) &  vertMedioArriba(C,F,Jugador,R3) &  vertArriba(C,F,Jugador,R4) & .max([R1,R2,R3,R4],V).																																		
valorHor(C,F,Jugador,V) :- horIzquierda(C,F,Jugador,R1) & horMedioIzquierda(C,F,Jugador,R2) & horMedioDerecha(C,F,Jugador,R3) & horDerecha(C,F,Jugador,R4) & .max([R1,R2,R3,R4],V).																																			
valorAsc(C,F,Jugador,V) :- ascIzquierda(C,F,Jugador,R1) & ascMedioIzquierda(C,F,Jugador,R2) & ascMedioDerecha(C,F,Jugador,R3) & ascDerecha(C,F,Jugador,R4) & .max([R1,R2,R3,R4],V).																																				
valorDesc(C,F,Jugador,V) :- descIzquierda(C,F,Jugador,R1) & descMedioIzquierda(C,F,Jugador,R2) & descMedioDerecha(C,F,Jugador,R3) & descDerecha(C,F,Jugador,R4) & .max([R1,R2,R3,R4],V).	

//Infiere en R el nmero de fichas de Jugador que se alinearn en vertical si Jugador ocupa la posicin (C,F)
vertArriba(C,F,Jugador,R) :- (F-3)>=0 & tablero(C,F,0) & tablero(C,F-1,Y) & tablero(C,F-2,Z) & tablero(C,F-3,W) & num([Y,Z,W],Jugador, [], R).
vertMedioArriba(C,F,Jugador,R) :- (F-2)>=0 &(F+1)<8 & tablero(C,F+1,Y) & tablero(C,F,0) & tablero(C,F-1,Z) & tablero(C,F-2,W) & num([Y,Z,W],Jugador, [], R).
vertMedioAbajo(C,F,Jugador,R) :- (F-1)>=0 & (F+2)<8 & tablero(C,F+2,Y) & tablero(C,F+1,Z) & tablero(C,F,0) & tablero(C,F-1,W) & num([Y,Z,W],Jugador, [], R).
vertAbajo(C,F,Jugador,R) :- (F+3)<8 & tablero(C,F+3,Y) & tablero(C,F+2,Z) & tablero(C,F+1,W) & tablero(C,F,0) & num([Y,Z,W],Jugador, [], R).
vertAbajo(C,F,Jugador,0).
vertMedioAbajo(C,F,Jugador,0).
vertMedioArriba(C,F,Jugador,0).
vertArriba(C,F,Jugador,0).

//Infiere en R el nmero de fichas de Jugador que se alinearn en horizontal si Jugador ocupa la posicin (C,F)
horIzquierda(C,F,Jugador,R) :- (C-3)>=0 & tablero(C-3,F,W) & tablero(C-2,F,Z) & tablero(C-1,F,Y) & tablero(C,F,0) & num([Y,Z,W],Jugador, [], R).
horMedioIzquierda(C,F,Jugador,R) :- (C-2)>=0 & (C+1)<8 & tablero(C-2,F,W) & tablero(C-1,F,Z) & tablero(C,F,0) & tablero(C+1,F,Y) & num([Y,Z,W],Jugador, [], R).
horMedioDerecha(C,F,Jugador,R) :- (C-1)>=0 & (C+2)<8 & tablero(C-1,F,W) & tablero(C,F,0) & tablero(C+1,F,Z) & tablero(C+2,F,Y) & num([Y,Z,W],Jugador, [], R).
horDerecha(C,F,Jugador,R) :- (C+3)<8 & tablero(C,F,0) & tablero(C+1,F,Y) & tablero(C+2,F,Z) & tablero(C+3,F,W) & num([Y,Z,W],Jugador, [], R).
horIzquierda(C,F,Jugador,0).
horMedioIzquierda(C,F,Jugador,0).
horMedioDerecha(C,F,Jugador,0).
horDerecha(C,F,Jugador,0).

//Infiere en R el nmero de fichas de Jugador que se alinearn en diagonal descendiente si Jugador ocupa la posicin (C,F)
descIzquierda(C,F,Jugador,R) :- (C-3)>=0 & (F-3)>=0 & tablero(C-3,F-3,W) & tablero(C-2,F-2,Z) & tablero(C-1,F-1,Y) & tablero(C,F,0) & num([Y,Z,W],Jugador, [], R).
descMedioIzquierda(C,F,Jugador,R) :- (C-2)>=0 & (F-2)>=0 & (C+1)<8 & (F+1)<8 & tablero(C-2,F-2,W) & tablero(C-1,F-1,Z) & tablero(C,F,0) & tablero(C+1,F+1,Y) & num([Y,Z,W],Jugador, [], R).
descMedioDerecha(C,F,Jugador,R) :- (C-1)>=0 & (F-1)>=0 & (C+2)<8 & (F+2)<8 & tablero(C-1,F-1,W) & tablero(C,F,0) & tablero(C+1,F+1,Z) & tablero(C+2,F+2,Y) & num([Y,Z,W],Jugador, [], R).
descDerecha(C,F,Jugador,R) :- (C+3)<8 & (F+3)<8 & tablero(C,F,0) & tablero(C+1,F+1,Y) & tablero(C+2,F+2,Z) & tablero(C+3,F+3,W) & num([Y,Z,W],Jugador, [], R).
descIzquierda(C,F,Jugador,0).
descMedioIzquierda(C,F,Jugador,0).
descMedioDerecha(C,F,Jugador,0).
descDerecha(C,F,Jugador,0).

//Infiere en R el nmero de fichas de Jugador que se alinearn en diagonal ascendiente si Jugador ocupa la posicin (C,F)
ascIzquierda(C,F,Jugador,R) :- (C+3)<8 & (F-3)>=0 & tablero(C,F,0) & tablero(C+1,F-1,Y) & tablero(C+2,F-2,Z) & tablero(C+3,F-3,W) & num([Y,Z,W],Jugador, [], R).
ascMedioIzquierda(C,F,Jugador,R) :- (C-1)>=0 & (F-2)>=0 & (C+2)<8 & (F+1)<8 & tablero(C-1,F+1,Y) & tablero(C,F,0) & tablero(C+1,F-1,Z) & tablero(C+2,F-2,W) & num([Y,Z,W],Jugador, [], R).
ascMedioDerecha(C,F,Jugador,R) :- (C-2)>=0 & (F-1)>=0 & (C+1)<8 & (F+2)<8 & tablero(C-2,F+2,Y) & tablero(C-1,F+1,Z) & tablero(C,F,0) & tablero(C+1,F-1,W) & num([Y,Z,W],Jugador, [], R).
ascDerecha(C,F,Jugador,R) :- (C-3)>=0 & (F+3)<8 & tablero(C-3,F+3,W) & tablero(C-2,F+2,Z) & tablero(C-1,F+1,Y) & tablero(C,F,0) & num([Y,Z,W],Jugador, [], R).
ascIzquierda(C,F,Jugador,0).
ascMedioIzquierda(C,F,Jugador,0).
ascMedioDerecha(C,F,Jugador,0).
ascDerecha(C,F,Jugador,0).

//Comprueba cuantas fichas de Jugador se juntan
num([],Jugador,N,R) :- .length(N,R).
num([Car|Cdr] ,Jugador, N, R) :- Car==Jugador & num(Cdr,Jugador, [1|N], R).
num([Car|Cdr] ,Jugador, N, R) :- Car==0 & num(Cdr,Jugador, N, R).
num([Car|Cdr] ,Jugador, N, 0).

/* Gestin de las percepciones (datos enviados por tablero) */
+tablero(C,F,Duenho)[source(percept)].
+tablero(C,F,Duenho)[source(Sender)]<-
	-tablero(C,F,Duenho)[source(Sender)].
	
+estrategia(Estrategia)[source(percept)].
+estrategia(Estrategia)[source(Sender)]<-
	-estrategia(Estrategia)[source(Sender)].

+turno(Player)[source(percept)]: jugadorActual(Player) & estrategia(Estrategia) <-
	.wait(500);
	!escogerMovimiento(Estrategia).
+turno(Player)[source(Sender)] <-
	-turno(Player)[source(Sender)].

/* Planes: Ejecucin de la estrategia */
+!escogerMovimiento(Estrategia)[source(self)]: funcionMovimiento(X,Estrategia) <-
	put(X).

/* Solo el propio agente puede ejecutar el plan */
+!escogerMovimiento(Estrategia)[source(Sender)].
